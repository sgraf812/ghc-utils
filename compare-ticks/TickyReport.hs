{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NamedFieldPuns #-}

module TickyReport
  ( parseReport
  , TickyReport(..)
  , TickyFrame(..)
  , TickyStats(..)
  , ClosureKind(..)
  , ModuleName(..)
  , pprModuleName
  , StgName(..)
  , stgnName
  , stgnDefiningModule
  , pprStgName
  ) where

import Data.Maybe
import Text.Trifecta
import Control.Monad (void)
import Control.Applicative
import Data.Monoid
import qualified Data.Text as T
import Prelude
import Debug.Trace

_trcM :: Applicative f => String -> f ()
_trcM s
  | id False  = traceM s
  | otherwise = pure ()

data TickyReport = TickyReport { frames :: [TickyFrame] }
                 deriving (Show)

data TickyFrame = TickyFrame { stats     :: TickyStats
                             , arguments :: String
                             , stgName   :: StgName
                             }
                deriving (Show)

data TickyStats = TickyStats { entries   :: Integer
                             , alloc     :: Integer
                             , allocd    :: Integer
                             }
                deriving (Show, Eq)

instance Semigroup TickyStats where
    TickyStats a b c <> TickyStats x y z = TickyStats (a+x) (b+y) (c+z)

instance Monoid TickyStats where
    mempty = TickyStats 0 0 0

data ClosureKind = Func { singleEntry :: Bool }
                 | Thunk { stdThunk :: Bool, singleEntry :: Bool }
                 | Con String
                 deriving (Show, Eq)

-- | The name of a module
data ModuleName = ModName { modulePackage :: String
                          , moduleName    :: String
                          }
                deriving (Show)

noModule :: ModuleName
noModule = ModName "" "<no module>"

pprModuleName :: ModuleName -> String
pprModuleName (ModName pkg name) = pkg<>":"<>name

-- | An STG name
--
-- An exported name like @ghc-7.11:CmdLineParser.runCmdLine{v rWL}@ is,
--
-- @
-- StgName { definingModule = Just $ ModName (Just "ghc-7.11") "CmdLineParser"
--         , name = "runCmdLine"
--         , signature = "v rWL"
--         , parent = Nothing
--         }
-- @
--
-- A non-exported name like @sat_s5EP{v} (ghc-7.11:CmdLineParser) in rXu@ is,
--
-- @
-- StgName { definingModule = Nothing
--         , name = "sat_s5EP"
--         , signature = "v"
--         , parent = "rXu"
--         }
-- @
data StgName = StgName { stgnExported       :: Bool
                       , _stgnDefiningModule :: ModuleName
                       , _stgnName          :: String
                       , stgnSignature      :: String
                       , stgnParent         :: Maybe String
                       }
             | Top
             deriving (Show)

stgnName :: StgName -> String
stgnName (StgName {_stgnName=n}) = n
stgnName Top = "Top"

pprStgName :: StgName -> String
pprStgName Top = "Top"
pprStgName (StgName _exported modname name _sig _parent) =
    name<>" ("<>pprModuleName modname<>")"

stgnDefiningModule :: StgName -> ModuleName
stgnDefiningModule Top = ModName "" ""
stgnDefiningModule (StgName _ modname _ _ _) = modname

parseReport :: T.Text -> TickyReport
parseReport s =
    let ls = T.lines s
        tableLines = case dropWhile (\l -> not $ "---------------------" `T.isPrefixOf` l) ls of
                       _:xs -> xs
                       _    -> error "parseReport: Parse error: Failed to find beginning of report"
        parseIt str = case parseString parseFrame mempty (T.unpack str) of
          Success a -> a
          Failure err_ -> error $ show (_errDoc err_)
    in TickyReport $ map parseIt
                   $ filter (not . T.null)
                   $ takeWhile (\l -> not $ "*****************" `T.isPrefixOf` l)
                   $ tableLines

spacesThen :: Parser a -> Parser a
spacesThen parser = skipMany space *> parser

named :: String -> Parser a -> Parser a
named n p = p <?> n

parseFrame :: Parser TickyFrame
parseFrame = do
    stats <- TickyStats
        <$> spacesThen integer
        <*> spacesThen integer
        <*> spacesThen integer

    TickyFrame stats
     <$> spacesThen (do n <- integer
                        spaces
                        -- sometimes this field is wider than its allotted width
                        case n of
                          0 -> return ""
                          _ -> many $ noneOf " ")
     <*> spacesThen parseStgName
     <*  eof

parseClosureKind :: Parser ClosureKind
parseClosureKind = choice
    [ Func False <$ text "(fun)"
    , Func True  <$ text "(fun,se)"
    , Thunk False False <$ text "(thk)"
    , Thunk False True  <$ text "(thk,se)"
    , Thunk True  False <$ text "(thk,std)"
    , Thunk True  True  <$ text "(thk,se,std)"
    , Con "" <$ text "(con)"  -- prior to 8.12 we didn't emit the constructor name
    , do void $ text "(con: " -- e.g. (con: GHC.Num.Integer.IS{(w) d 65O})
         _trcM ("pck")
         conName <- parseConName
         _trcM ("pck " ++ show conName)
         void $ text ")"
         return (Con conName)
    ]

-- e.g. GHC.Num.Integer.IS{(w) d 65O}
parseConName :: Parser String
parseConName = do
  _trcM ("pcn")
  pkg <- fromMaybe "" <$> optional (try (many (noneOf ":.{")  <* char ':'))
  _trcM ("pcn:pkg " ++ show pkg)
  modAndCon <- some (noneOf "{")
  _trcM ("pcn:modC " ++ show modAndCon)
  annot <- braces $ some (noneOf "}")
  _trcM ("pcn:annot " ++ show annot)
  return $ concat [pkg, modAndCon, "{", annot, "}"]

parseStgName :: Parser StgName
parseStgName = topName <|> try nonExportedName <|> exportedName

funcName, sig :: Parser String
funcName = many $ alphaNum <|> oneOf "$=<>[]()+-,.#*|/_'!@"
sig = named "signature" $ braces $ many $ noneOf "}"

-- e.g. TOP
topName :: Parser StgName
topName = text "TOP" *> pure Top

-- e.g. ghc-7.11:CmdLineParser.runCmdLine{v rWL}
exportedName :: Parser StgName
exportedName = named "exported name" $ do
  modname <- parseModuleName
  _name <- funcName
  _sig <- sig
  spaces
  void $ optional parseClosureKind
  return $ StgName True modname _name _sig Nothing

-- e.g. sat_sbMg{v} (main@main:Main) (con: GHC.Num.Integer.IS{(w) d 65O}) in sbMh
nonExportedName :: Parser StgName
nonExportedName = named "non-exported name" $ do
  _trcM ("trying nen")
  _name <- many $ noneOf "{"
  _sig <- sig
  spaces
  modname <- fmap (fromMaybe noModule) $ optional $ try $ parens parseModuleName
  _trcM ("nen:modname " ++ show modname)
  spaces
  _clokind <- optional parseClosureKind
  _trcM ("nen:clokind " ++ show _clokind)
  parent <- optional $ spaces *> text "in" *> spaces *> funcName
  _trcM ("nen:parent " ++ show parent)
  return $ StgName False modname _name _sig parent

startsWith :: Parser a -> Parser a -> Parser [a]
startsWith first others = (:) <$> first <*> many others

parseModuleName :: Parser ModuleName
parseModuleName = named "module name" $ try qualModuleName <|> unqualModuleName
  where
    qualModuleName = do
        package <- packageName
        void $ optional $ char '@' >> packageName  -- what is this?
        void (text "::") <|> void (char ':') -- not sure what :: is all about
        name <- modName
        return $ ModName package name
    unqualModuleName = do
        name <- modName
        return $ ModName "" name
    packageName = many $ alphaNum <|> oneOf "-_."
    modName = startsWith upper $ alphaNum <|> oneOf "_."
