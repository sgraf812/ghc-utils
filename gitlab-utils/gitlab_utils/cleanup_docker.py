#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gitlab
from typing import Set, List
from pathlib import Path
import subprocess
import re

DockerTag = str

def git_output(args: List[str], cwd: Path) -> str:
    return subprocess.check_output(['git'] + args, encoding='UTF-8', cwd=cwd)

def find_live_tags(ghc_dir: Path) -> Set[DockerTag]:
    relevant_commits = [
        commit
        for commit in git_output(['log', '--format=%H', '--since=6 months ago', '.gitlab-ci.yml'], cwd=ghc_dir).split('\n')
    ]

    live_tags = set()
    r = re.compile('DOCKER_REV: *([0-9a-f]+)')
    for commit in relevant_commits:
        s = git_output(['show', f'{commit}:.gitlab-ci.yml'], cwd=ghc_dir)
        for tag in r.findall(s):
            live_tags.add(tag)

    return live_tags

def run(ghc_dir: Path, dry_run: bool) -> None:
    live_tags = find_live_tags(ghc_dir)
    live_tags.add('latest')
    tag_list = '\n  '.join(live_tags)
    print(f"live tags:")
    print(f"  {tag_list}")

    g = gitlab.Gitlab.from_config()
    proj = g.projects.get('ghc/ci-images')
    deleted = 0
    for repo in proj.repositories.list():
        print(f'Processing {repo.name}...')
        for tag in repo.tags.list():
            if tag.name not in live_tags:
                print(f'tag {repo.name}:{tag.name} is dead')
                if not dry_run:
                    tag.delete()
                    deleted += 1

    print(f"deleted {deleted} tags. Don't forget to garbage-collect.")

def main() -> None:
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--dry-run', action='store_true')
    parser.add_argument('--ghc-dir', default='/opt/exp/ghc/ghc-landing')
    args = parser.parse_args()
    run(args.ghc_dir, args.dry_run)

if __name__ == '__main__':
    main()
