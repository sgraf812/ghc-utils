let
  nixpkgs = fetchGit {
    url = https://github.com/NixOS/nixpkgs;
    rev = "5607e74b1dd2c9bf19fcb35ddf5ee68b3228f67c";
    ref = "release-20.03";
  };
in with (import nixpkgs {});
let
  fetch-gitlab = { buildPythonPackage, python-gitlab }:
    buildPythonPackage {
      pname = "gitlab-utils";
      version = "0.0.1";
      src = ./.;
      propagatedBuildInputs = [ python3Packages.python-gitlab ];
      preferLocalBuild = true;
    };
in 
python3Packages.callPackage fetch-gitlab { }
