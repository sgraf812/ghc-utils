for n in `seq 0 10`; do
  let m=n+1
  start_t="$(date -I -d "$m years ago")"
  end_t="$(date -I -d "$n years ago")"
  commits="$(git log --pretty="%H" --oneline --since $start_t --until $end_t master)"
  n_commits="$(echo "$commits" | wc -l)"
  git log --numstat --format="" --since $start_t --until $end_t master | awk '{files += 1}{ins += $1}{del += $2} END{print "total: "files" files, "ins" insertions(+) "del" deletions(-)"}'

  echo "$n    $start_t .. $end_t     $n_commits"
done


